/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Memos
 */
public class StoreService {
    private static ArrayList<Store> storeList = new ArrayList<>();
    private static Store store = null;
    
    static {

    }
    public static boolean addStore(Store store){
        storeList.add(store);
        save();
        return true;
    }
    public static boolean delStore(Store store){
        storeList.remove(store);
        save();
        return true;
    }
    public static boolean updateStore(int index, Store store){
        storeList.set(index, store);
        save();
        return true;
    }
    public static ArrayList<Store> getStores(){
        return storeList;
    }
    public static Store getStores(int index){
        return storeList.get(index);
    }

    public static boolean delStore(int index) {
        storeList.remove(index);
        save();
        return true;
    }
    public static boolean Clear(){
        storeList.removeAll(storeList);
        save();
        return true;
    }
    public static int size(){
        return storeList.size();
    }
    public static void save(){
        File file = null;
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
        try
        {
            file = new File("name.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(storeList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
    public static void load(){
        File file = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
        try
        {
            file = new File("name.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            storeList = (ArrayList<Store>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex){
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex){
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex){
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
